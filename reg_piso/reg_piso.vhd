LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

entity reg_piso is
generic(K:natural:=8);
  port (
		   d :in std_logic_vector(K-1 downto 0);
		  ld :in std_logic;
		  en :in std_logic;
		 clk :in std_logic;
	     rst :in std_logic;
		  q :out std_logic);
end reg_piso ;

architecture Behavioral of reg_piso is
	signal s_k_reg, s_k_next : std_logic_vector(K downto 0);
begin

-- logica secuencial
	process ( clk , rst )
		begin
		if (rst = '1') then
			s_k_reg <= (others =>'0');
		elsif( rising_edge (clk) ) then
			s_k_reg <= s_k_next;
		end if ;
	end process ;    

	-- logica de rotacion	
	s_k_next(0) <= s_k_reg(K); 
	s_k_next(K downto 1) <= d when ld='1' else
                          s_k_reg(K-1 downto 0) when en='1' else
                          s_k_reg(K downto 1);	 

	-- logica de salida
	q <= s_k_reg(K);
end architecture;