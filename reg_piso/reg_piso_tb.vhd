library work;
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
use std.env.stop; --Posibilita el stop sin recurrir al failure
 
ENTITY to_test_tb IS
END to_test_tb;
 
ARCHITECTURE behavior OF to_test_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT reg_piso
    PORT(
         d : IN  std_logic_vector(7 downto 0);
         ld : IN  std_logic;
         en : IN  std_logic;
         clk : IN  std_logic;
         rst : IN  std_logic;
         q : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal d_t : std_logic_vector(7 downto 0) := (others => '0');
   signal ld_t : std_logic := '0';
   signal en_t : std_logic := '0';
   signal clk_t : std_logic := '0';
   signal rst_t : std_logic := '0';

  --Outputs
   signal q_t : std_logic;

   -- Clock period definitions
   constant Tclk : time := 10 ns;
   
 
BEGIN
 
  -- Instantiate the Unit Under Test (UUT)
   uut: entity work.reg_piso PORT MAP (
          d => d_t,
          ld => ld_t,
          en => en_t,
          clk => clk_t,
          rst => rst_t,
          q => q_t
        );

   -- Clock process definitions
   clk_process :process
   begin
    clk_t <= '1';
    wait for Tclk/2;
    clk_t <= '0';
    wait for Tclk/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin    
      rst_t <='1';
      wait for 100 ns;  
      rst_t <='0';
      d_t <= "11001100"; -- siempre en 1
      en_t <= '1';
      wait for Tclk;
      ld_t <= '1';
      wait for Tclk;
      ld_t <= '0';
      wait for Tclk*8;
      en_t <= '0';
      wait for Tclk*8;

      --report "fin de la simulacion"
    --severity failure;
      stop;
   end process;

   chech : process
   -- Errors count
      variable   errors: integer := 0;
   begin
      wait for 100 ns; 
      wait for Tclk;
      wait for Tclk;
      wait until falling_edge(clk_t);
      report "inicio del chequeo"& LF & LF;
      for i in 7 downto 0 loop
        if d_t(i)/=q_t then
                report LF &
               "se esperaba" & std_logic'image(d_t(i)) & LF &
               "se obtuvo" & std_logic'image(q_t) & LF & LF;
               errors := errors + 1;
      else
            report LF &
               "salida esperada" & LF & LF;
        end if ;
    wait for Tclk;
      end loop ; -- check
    wait for Tclk*7;
    if errors /= 0 then
      report LF & "ERRORES ENCONTRADOS" & LF & LF;
    else
      report LF & "SIMULACION EXITOSA" & LF & LF;
    end if ; 
   end process ; -- chech

END;
