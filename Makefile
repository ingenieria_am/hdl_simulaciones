TB ?= to_test_tb# --nombre de la entidad del test bench
F ?= to_test

ci:
	ghdl -i --std=08 *.vhd
	ghdl -s --std=08 *.vhd
	ghdl -m --std=08 --ieee=synopsys -frelaxed $(TB)
	ghdl -r --std=08 --ieee=synopsys -frelaxed $(TB) --wave=$(TB).ghw

.ONESHELL:
local:
	cd $(PWD)/$(F)
	ghdl -i --std=08 *.vhd
	ghdl -s --std=08 *.vhd
	ghdl -m --std=08 --ieee=synopsys -frelaxed $(TB)
	ghdl -r --std=08 --ieee=synopsys -frelaxed $(TB) --wave=$(TB).ghw 

sim:
	cd $(PWD)/$(F)
	gtkwave $(TB).ghw 

clean:
	-rm -f *.o *.ghw work-obj08.cf

clean_folder:
	cd $(PWD)/$(F)
	-rm -f *.o *.ghw work-obj08.cf

help:
	@echo Debe especificar el directorio F=some_folder y entidad TB_ENTITY=entity_tb '\n',esto genera los archivos de simulación.'\n' 
	@echo '\t'- ejemplo: '\n\t\t'make local F=tp2/ej1 TB=ej1_tb
	@echo '\t'- para ver formas de onda, tras generar archivos de simulación: '\n\t\t'make sim F=tp2/ej1 TB=ej1_tb
	@echo '\t'- previo al commit, para borrar archivos extra si se hizo un test local: '\n\t\t'make clean
	@echo '\t'- previo al commit, para borrar archivos extra: '\n\t\t'make clean_folder F=tp2/ej1
