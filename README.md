# Simulaciones mediante CI - gitlab

[![CI](https://about.gitlab.com/images/ci/ci-cd-test-deploy-illustration_2x.png)]()

### Herramienta de testing para proyectos HDL

  - Basado en ghdl
  - Vista de formas de onda mediante GTKwave
  - Admite simulaciones con CocoTB

### Instrucciones
---
***Forma de trabajo en la pc (forma completa de simulación):***
- Debera tener instalado ghdl y gtkwave:
```sh 
sudo apt-get install ghdl gtkwave
```
- Una carpeta por diseño
- Dicha carpeta deberá contener el diseño y su test bench 
```sh 
desing.vhd  
desing_tb.vhd
```
- con la instrucción **make local** se realiza la simulación la cual informa errores, por ejemplo:
```sh 
make local F=tp2/ej1 TB=ej1_tb
```
- con **make sim** se visualizan las formas de onda mediante gtkwave, por ejemplo:
```sh 
make sim F=tp2/ej1
```

- previo a realizar el commit debera utilizar **make clean** para limpiar archivos generados, por ejemplo:
```sh 
make clean F=tp2/ej1
```
***forma de trabajo con CI***

- en el directorio de trabajo, colocar los archivos **vhd** antes descriptos.
- se imponen los nombres **to_test.vhd** y **to_test_tb.vhd** y la entidad del test_bench debe respetar este nombre:
```vhdl
ENTITY to_test_tb IS
END to_test_tb;
 
ARCHITECTURE behavior OF to_test_tb IS 
...
``` 
- esta forma de trabajo hará la simulación sobre estos archivos "sueltos", con lo cual se comprende esta como una modalidad extra que se ofrece, luego podrá ubicar los archivos en la carpeta correspondiene.
- el commit, por tanto, deberá contener la información adecuada sobre el circuito a los efectos de mantener el orden.